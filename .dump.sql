CREATE TABLE `tokens` (
  `id` int(11) NOT NULL,
  `token` varchar(48) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(32) DEFAULT NULL,
  `email` varchar(64) NOT NULL,
  `phone` varchar(32) NOT NULL,
  `instagram` varchar(32) NOT NULL,
  `num` tinyint(3) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `categories` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `alias` varchar(1) NOT NULL,
  `desc` text NOT NULL,
  `num` smallint(6) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `categories` (`id`, `name`, `alias`, `desc`, `num`) VALUES
(1, 'Lobster Mutiara - Panulirus Ornatus', 'm', 'Lobster Mutiara adalah salah satu lobster paling favorit di Indonesia. Karena tekstur dagingnya yang lembut, tebal dan manis, nggak heran kalau lobster ini sering juga dijadikan menu sashimi. Yang jelas lobster Mutiara bisa bikin kamu nggak akan bisa lupa dengan kelezatan rasanya. \r\nSeperti Panulirus Ornatus, karena kamu disukai banyak orang, nggak jarang banyak orang yang senang berada di dekatmu. Tapi hati-hati jangan sampai kamu malah bikin baper banyak orang ya!', 0),
(2, 'Lobster Bambu - Panulirus Versicolor', 'b', 'Lobster bambu sesuai namanya, lobster ini memiliki warna hijau seperti bambu. Tapi di beberapa daerah, lobster ini juga disebut dengan nama lobster hijau. Lobster ini tinggal di daerah karang atau bebatuan di perairan jernih atau keruh yang disertai arus yang kuat  dengan kedalaman sekitar 4-16 meter. \r\nSeperti Panulirus Versicolor, kamu adalah seorang petualang sejati. Untuk mencari pasangan yang cocok kamu akan melakukannya sampai menemukan si dia yang paling pas di hati. Kamu cenderung orang yang setia dengan pasangan.', 0),
(3, 'Lobster Pasir - Panulirus Homarus', 'p', 'Lobster pasir memiliki warna tubuh yang lebih gelap. Umumnya berwarna cokelat, terdapat bintik putih memanjang sampai pangkal ekor. Ukuran tubuh lobster pasir jantan lebih besar dibanding betina. Lobster ini disebut-sebut mempunyai daya tahan hidup yang cukup baik. Sebab, hewan ini menempati daerah dengan karakteristik ombak besar dan berpasir. \r\nSeperti Panulirus Homarus, kamu adalah orang yang tahan banting. Kamu adalah tipe yang rela melakukan apa saja demi pasangan kamu. Bisa dibilang kamu adalah si bucin.', 0),
(4, 'Lobster Capit Merah - Cherax Quadricarinatus', 'c', 'Lobster capit merah atau red claw adalah lobster air tawar yang memiliki ciri-ciri utama berwarna biru kehijauan serta pada kedua capitnya terdapat garis berwarna merah. Lobster jenis ini biasanya dijadikan sebagai hewan konsumsi karena pertumbuhannya yang relatif lebih cepat dari pada lobster lainnya.  \r\nSeperti Cherax Quadricarinatus, kamu itu adalah pribadi yang unik. Mungkin akan agak susah menemukan pasangan yang benar-benar cocok untuk kamu. Tapi kerennya kamu setelah menemukan pasangan yang tepat adalah si dia bisa dibikin klepek-klepek sama pesona yang kamu miliki.', 0);

CREATE TABLE `answers` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `category_id` int(11) UNSIGNED NOT NULL,
  `answer` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `category_id` (`category_id`);

ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `tokens`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `answers`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `categories`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

ALTER TABLE `tokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `answers`
  ADD CONSTRAINT `answers_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `answers_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);

COMMIT;