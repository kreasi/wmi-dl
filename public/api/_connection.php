<?php
error_reporting(E_ALL);
ini_set('display_errors', true);
ini_set('display_startup_errors', true);

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    header('Access-Control-Allow-Origin: *');
    header(
        'Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS'
    );
    header('Access-Control-Allow-Headers: token, Content-Type');
    header('Access-Control-Max-Age: 1728000');
    header('Content-Length: 0');
    header('Content-Type: text/plain');
    die();
}

header('Access-Control-Allow-Origin: *');

include_once '_config.php';
include_once '_dbclass.php';

$db = new db($dbhost, $dbuser, $dbpass, $dbname);
