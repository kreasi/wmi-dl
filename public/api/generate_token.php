<?php
/*
 * [GET] /api/index.php/generate_token
 */
$token = md5(time() + rand(100, 999));

$db->query('INSERT INTO `tokens` (`token`) VALUES (?)', $token);

echo json_encode([
    'success' => true,
    'token' => $token,
]);
