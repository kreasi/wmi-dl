<?php
/*
 * [GET] /api/index.php/get_category?api_token=<api_token>
 */
if (isset($_GET['api_token']) && $_GET['api_token'] == $apitoken) {
    $categories = $db->query('SELECT * FROM `categories`')->fetchAll();
    echo json_encode($categories);
} else {
    echo json_encode([
        'success' => false,
        'message' => 'Unauthorized.',
    ]);
}
