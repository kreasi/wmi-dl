<?php
/*
 * [GET] /api/index.php/get_user?api_token=<api_token>
 */
if (isset($_GET['api_token']) && $_GET['api_token'] == $apitoken) {
    $users = $db->query('SELECT * FROM `users`')->fetchAll();
    echo json_encode($users);
} else {
    echo json_encode([
        'success' => false,
        'message' => 'Unauthorized.',
    ]);
}
