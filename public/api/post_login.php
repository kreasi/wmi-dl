<?php
/*
 * [POST] /api/index.php/post_login
 */
$data = json_decode(file_get_contents('php://input'), true);

if ($data && $data['username'] && $data['password']) {
    if ($data['username'] == $admuser && $data['password'] == $admpass) {
        echo json_encode([
            'success' => true,
            'message' => 'Welcome, Admin!',
        ]);
    } else {
        echo json_encode([
            'success' => false,
            'message' => 'Invalid username and/or password.',
        ]);
    }
} else {
    echo json_encode([
        'success' => false,
        'message' => 'Incomplete data.',
    ]);
}
