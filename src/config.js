export default {
  BASE_API: 'api/index.php/',
  // BASE_API: 'http://localhost:5001/api/index.php/',
  API_TOKEN: 'wmi2021',
  APP_NAME: 'Dua Lipa',
  APP_TITLE: 'Jenis Lobster Apakah Kamu?',
  QUESTIONS: [
    {
      label:
        'Gebetan kamu tiba-tiba ngajak kamu nge-date, apa yang bakal kamu lakukan?',
      type: 'text',
      options: [
        {
          label: 'Mau lah, lumayan ditraktir.',
          alias: 'm'
        },
        {
          label: 'Cek dulu kira-kira ada maunya apa nggak.',
          alias: 'b'
        },
        {
          label: 'Kemanapun kamu ajak aku ikut.',
          alias: 'p'
        },
        {
          label: 'Bingung, diemin dulu aja deh.',
          alias: 'c'
        }
      ]
    },
    {
      label:
        'Di depan mata ada yang ngasih makanan favorit kamu, apa yang bakal kamu lakuin?',
      type: 'text',
      options: [
        {
          label: 'Sikat langsung di tempat.',
          alias: 'b'
        },
        {
          label: 'Bungkus, makan di rumah.',
          alias: 'c'
        },
        {
          label: 'Nawarin makanan ke gebetan.',
          alias: 'p'
        },
        {
          label: 'Bilang makasih ke semua orang.',
          alias: 'm'
        }
      ]
    },
    {
      label: 'Emoji apa yang paling menggambarkan kamu saat ini?',
      type: 'icon',
      options: [
        {
          label: '❤️',
          alias: 'p'
        },
        {
          label: '👽',
          alias: 'c'
        },
        {
          label: '😎',
          alias: 'm'
        },
        {
          label: '🦁',
          alias: 'b'
        }
      ]
    },
    {
      label:
        'Dalam waktu yang bersamaan, gebetan dan temen kamu ngajak nongkrong, mana yang kamu pilih?',
      type: 'text',
      options: [
        {
          label: 'Daripada drama, rebahan aja di rumah.',
          alias: 'c'
        },
        {
          label: 'Bawa gebetan ke tongkrongan temen.',
          alias: 'm'
        },
        {
          label: 'Temenlah, persahabatan nomor satu.',
          alias: 'b'
        },
        {
          label: 'Gebetan dong, temen pasti bisa ngertiin.',
          alias: 'p'
        }
      ]
    },
    {
      label:
        'Temen kamu nikah, apa yang bakal kamu siapin untuk datang ke acara resepsinya?',
      type: 'text',
      options: [
        {
          label: 'Semprot parfum yang banyak.',
          alias: 'm'
        },
        {
          label: 'Rantang buat bungkus makanan.',
          alias: 'c'
        },
        {
          label: 'Ngajak gebetan pake baju couple.',
          alias: 'p'
        },
        {
          label: 'Yang penting baju yang nyaman.',
          alias: 'b'
        }
      ]
    }
  ]
}
