import Index from './routes/Index.svelte'
import Question from './routes/Question.svelte'
import Result from './routes/Result.svelte'
import Reset from './routes/Reset.svelte'
import Terms from './routes/Terms.svelte'
import Login from './routes/Login.svelte'
import Admin from './routes/Admin.svelte'
import NotFound from './routes/NotFound.svelte'

export default {
  '/': Index,
  '/question': Question,
  '/result': Result,
  '/reset': Reset,
  '/terms': Terms,
  '/login': Login,
  '/admin': Admin,
  '*': NotFound
}
